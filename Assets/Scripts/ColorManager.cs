﻿using UnityEngine;

public class ColorManager : MonoBehaviour {

    public Color[] platformColors = new Color[6], platformPlaneColors = new Color[6], wallColors = new Color[6], wallPlaneColors = new Color[6], equatorColors = new Color[6],
                   fogColors = new Color[6];
    public static int nColor = 0;
    public bool changeColors = false, changeWallColor = false, changePlatformColor = false;
    float aTime, timeSwitch = 4;
    public Material wallMaterial, wallPlaneMaterial, platformMaterial, platformPlaneMaterial;

    void Start() {
        RenderSettings.fogColor = fogColors[nColor];
        RenderSettings.ambientEquatorColor = equatorColors[nColor];
        wallMaterial.color = wallColors[nColor];
        wallPlaneMaterial.color = wallPlaneColors[nColor];
        platformMaterial.color = platformColors[nColor];
        platformPlaneMaterial.color = platformPlaneColors[nColor];
    }

    void Update() {
        if (changeColors) {
            aTime += Time.deltaTime;
            RenderSettings.fogColor = Color.Lerp(RenderSettings.fogColor, fogColors[nColor], aTime / timeSwitch);
            RenderSettings.ambientEquatorColor = Color.Lerp(RenderSettings.ambientEquatorColor, equatorColors[nColor], aTime / timeSwitch);
            wallMaterial.color = Color.Lerp(wallMaterial.color, wallColors[nColor], aTime / timeSwitch);
            wallPlaneMaterial.color = Color.Lerp(wallPlaneMaterial.color, wallPlaneColors[nColor], aTime / timeSwitch);
            platformMaterial.color = Color.Lerp(platformMaterial.color, platformColors[nColor], aTime / timeSwitch);
            platformPlaneMaterial.color = Color.Lerp(platformPlaneMaterial.color, platformPlaneColors[nColor], aTime / timeSwitch);
        }
    }

    public void StartSwitchColors() {
        changeColors = true;
        if (nColor >= 5) {
            nColor = 0;
        }
        else {
            nColor++;
        }
    }

    public void StopSwitchColors() {
        changeColors = false;
        aTime = 0;
    }

    public void SetObjectColor(string objectName) {
        if (objectName == "Platform") {
            platformMaterial.color = platformColors[nColor];
            platformPlaneMaterial.color = platformPlaneColors[nColor];
        }
        if (objectName == "Wall") {
            wallMaterial.color = wallColors[nColor];
            wallPlaneMaterial.color = wallPlaneColors[nColor];
        }
    }
}
