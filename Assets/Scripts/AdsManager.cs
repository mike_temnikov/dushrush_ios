﻿using UnityEngine;
//using GoogleMobileAds.Api;
using System.Collections;

public class AdsManager : MonoBehaviour {

    //static InterstitialAd interstitial, startInterstitial;
    public static bool showStartInterstitial = true, showInterstitial = false, first = true, previusState;
    public GameObject downloadScreen, startScreen, deadScreen;
    public static float time = 0;
    StartScreenController startScreenController;
    DeadScreenController deadScreenController;

    void Start() {
        startScreenController = startScreen.GetComponent<StartScreenController>();
        deadScreenController = deadScreen.GetComponent<DeadScreenController>();
        if (first && PlayerPrefs.GetInt("BuyNoAds") != 1) {
            RequestStartInterstitial();
            RequestInterstitial();
            first = false;
        }
        if (PlayerPrefs.GetInt("BuyNoAds") == 1) {
            if (first) {
                first = false;
                showStartInterstitial = false;
                startScreenController.View();
            }
        }
    }

    void Update() {
        if (PlayerPrefs.GetInt("BuyNoAds") != 1) {
            /*if (downloadScreen.activeSelf)
            {
                time += Time.deltaTime;
            }
            else
            {
                time = 0;
            }
            if (interstitial.IsLoaded() && showInterstitial)
            {
                StartCoroutine(Wait());
            }
            else if (showInterstitial)
            {
                downloadScreen.SetActive(true);
            }
            if (/*startInterstitial.IsLoaded() && showStartInterstitial)
            {
                //startInterstitial.Show();
                downloadScreen.SetActive(false);
                showStartInterstitial = false;
                startScreenController.View();
            }
            else if (showStartInterstitial)
            {
                downloadScreen.SetActive(true);
            }
            if (time > 5f)
            {
                downloadScreen.SetActive(false);
                if (showInterstitial)
                {
                    deadScreenController.View();
                    showInterstitial = false;
                }
                else
                {
                    startScreenController.View();
                    showStartInterstitial = false;
                }
            }*/
        }
    }

    void RequestStartInterstitial() {
        /*string adUnitId = "ca-app-pub-4030265201649343/1109549319";
        startInterstitial = new InterstitialAd(adUnitId);
        AdRequest request = new AdRequest.Builder().Build();
        startInterstitial.LoadAd(request);*/
    }

    void RequestInterstitial() {
        /*string adUnitId = "ca-app-pub-4030265201649343/2505557313";
        interstitial = new InterstitialAd(adUnitId);
        AdRequest request = new AdRequest.Builder().Build();
        interstitial.LoadAd(request);*/
    }

    IEnumerator Wait() {
        yield return new WaitForSeconds(2f);
        //interstitial.Show();
        downloadScreen.SetActive(false);
        RequestInterstitial();
        showInterstitial = false;
        deadScreenController.View();
    }
}
