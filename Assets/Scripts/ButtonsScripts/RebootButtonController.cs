﻿using UnityEngine;

public class RebootButtonController : MonoBehaviour {

    public GameObject deadScreenController;

    public void OnMouseDown() {
        deadScreenController.GetComponent<DeadScreenController>().Retry();
    }
}
