﻿using UnityEngine;

public class MusicButtonController : MonoBehaviour
{
    public GameObject noMusic;

    void Start() {
        if (!AudioManager.onMusic)
        {
            noMusic.SetActive(true);
        }
        else
        {
            noMusic.SetActive(false);
        }
    }

    public void OnMouseDown() {
        if (AudioManager.onMusic == true)
        {
            AudioManager.onMusic = false;
            noMusic.SetActive(true);
        }
        else
        {
            AudioManager.onMusic = true;
            noMusic.SetActive(false);
        }
    }
}
