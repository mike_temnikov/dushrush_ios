﻿using UnityEngine;

public class SoundButtonController : MonoBehaviour {

    public GameObject noSound, yesSound;

    void Start() {
        if (AudioManager.onSound)
        {
            yesSound.SetActive(true);
            noSound.SetActive(false);
        }
        else {
            yesSound.SetActive(false);
            noSound.SetActive(true);
        }
    }

    public void OnMouseDown() {
        if (AudioManager.onSound == true)
        {
            AudioManager.onSound = false;
            yesSound.SetActive(false);
            noSound.SetActive(true);
        }
        else
        {
            AudioManager.onSound = true;
            yesSound.SetActive(true);
            noSound.SetActive(false);
        }
    }
}
