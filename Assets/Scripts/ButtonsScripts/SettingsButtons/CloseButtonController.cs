﻿using UnityEngine;

public class CloseButtonController : MonoBehaviour {

    public GameObject settingsMenu, closeButton, startButton;

    public void Close() {
        settingsMenu.SetActive(false);
        closeButton.SetActive(false);
        startButton.SetActive(true);
    }
}
