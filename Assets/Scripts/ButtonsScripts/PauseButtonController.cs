﻿using UnityEngine;

public class PauseButtonController : MonoBehaviour {

    public GameObject playButton;

    public void OnMouseDown() {
        Time.timeScale = 0;
        gameObject.SetActive(false);
        playButton.SetActive(true);
    }

    public void PlayButton() {
        Time.timeScale = 1;
        gameObject.SetActive(true);
        playButton.SetActive(false);
    }
}
