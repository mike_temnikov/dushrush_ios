﻿using UnityEngine;


public class SettingsButtonController : MonoBehaviour {

    public GameObject settingsMenu, closeButton, startButton;

    public void OnMouseDown() {
        settingsMenu.SetActive(true);
        closeButton.SetActive(true);
        startButton.SetActive(false);
    }
}
