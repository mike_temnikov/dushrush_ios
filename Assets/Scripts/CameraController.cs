﻿using UnityEngine;

public class CameraController : MonoBehaviour {

    public GameObject player;
    Transform cameraTransform, playerTransform;
    float distance;
    Vector3 cameraPosition;

    void Start() {
        cameraTransform = GetComponent<Transform>();
        playerTransform = player.transform;
        distance = playerTransform.position.x - cameraTransform.position.x;
    }

    void Update() {
        cameraTransform.position = Vector3.Lerp(cameraTransform.position, new Vector3(playerTransform.position.x - distance,
                                                                                      cameraTransform.position.y,
                                                                                      cameraTransform.position.z), Time.deltaTime * 5);
    }
}
