﻿using UnityEngine;

public class AudioManager : MonoBehaviour {

    public AudioClip jump, touchPlatform, touchWall, dead, buttonClick, highscore;
    public static bool onSound = true, onMusic = true;
    private AudioSource audioSource;

    void Start() {
        audioSource = GetComponent<AudioSource>();
    }

    public void JumpSound() {
        if (onSound) {
            audioSource.clip = jump;
            audioSource.Play();
        }
    }

    public void WallTouchSound() {
        if (onSound)
        {
            audioSource.clip = touchWall;
            audioSource.Play();
        }
    }

    public void PlatformTouchSound() {
        if (onSound)
        {
            audioSource.clip = touchPlatform;
            audioSource.Play();
        }
    }

    public void ButtonClickSound() {
        if (onSound)
        {
            audioSource.clip = buttonClick;
            audioSource.Play();
        }
    }

    public void DeadSound() {
        if (onSound)
        {
            audioSource.clip = dead;
            audioSource.Play();
        }
    }

    public void HighscoreSound() {
        if (onSound) {
            audioSource.clip = highscore;
            audioSource.Play();
        }
    }
}
