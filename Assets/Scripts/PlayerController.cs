﻿using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {

    public Vector3 jumpImpulse, rebound, posPlatform;
    public float timeSwitch;
    public GameObject fog, deadPlayer, startScreen, deadScreen, gameScreen, scoreText, particles, audioManager, glassAudioManager, colorManager;
    public bool startGame = false;
    public System.Random random;
    private static int nGames = 0, nGame = 1;
    private float score;
    private Rigidbody rigidBody;
    private bool touchingPlatform = true, collision = false, collisionDamage = false;
    private RigidbodyConstraints previousConstaints;
    BoxCollider playerCollider;
    ColorManager colorManagerScript;
    Transform playerTransform, particlesTransform;
    AudioManager audioManagerComp;

    void Start()
    {
        playerCollider = GetComponent<BoxCollider>();
        playerTransform = GetComponent<Transform>();
        audioManagerComp = audioManager.GetComponent<AudioManager>();
        particlesTransform = particles.GetComponent<Transform>();
        colorManagerScript = colorManager.GetComponent<ColorManager>();
        if (!AdsManager.showStartInterstitial) {
            startScreen.GetComponent<StartScreenController>().View();
        }
        random = new System.Random();
        rigidBody = GetComponent<Rigidbody>();
        previousConstaints = rigidBody.constraints;
        rigidBody.constraints = RigidbodyConstraints.FreezeRotation;
        if (nGames == 3) {
            nGames = 0;
            colorManagerScript.StartSwitchColors();
        }
    }

    void Update()
    {
        if (playerTransform.position.y < 0.1)
        {
            StopGame();
        }
        if (System.Math.Abs(playerTransform.position.z) > 0.4) {
            fog.transform.parent = null;
            rigidBody.constraints = previousConstaints;
            startGame = false;
        }
        if (playerTransform.position.x - posPlatform.x > 4)
        {
            playerCollider.isTrigger = true;
        }
        if (playerTransform.position.x - posPlatform.x > 5)
        {
            playerCollider.isTrigger = false;
        }
    }

    void OnCollisionEnter(Collision other)
    {
        GameObject otherGameObject = other.gameObject;
        if (otherGameObject.tag == "Walls" && !collision)
        {
            audioManagerComp.WallTouchSound();
            CheckCollision(other);
            PlayerPrefs.SetInt("WallsCollisions", PlayerPrefs.GetInt("WallsCollisions") + 1);
            collision = true;
        }
        if (otherGameObject.name == "DamageBlock(Clone)" && !collisionDamage)
        {
            StopGame();
            collisionDamage = true;
        }
        if (otherGameObject.tag == "Platform" && startGame)
        {
            audioManagerComp.PlatformTouchSound();
            if ((score + 1) % 10 == 0)
            {
                nGames = 0;
                colorManagerScript.StartSwitchColors();

            } else {
                colorManagerScript.StopSwitchColors();
            }

            if (otherGameObject.transform.position != posPlatform) {
                AddScore(1);
            }
            posPlatform = other.transform.position;
            touchingPlatform = true;
            collision = false;
        }
    }

    void OnTriggerEnter(Collider other) {
        if (other.gameObject.name == "GlassBlock(Clone)") {
            glassAudioManager.GetComponent<GlassAudioManager>().GlassSound();
        }
    }

    public void Jump() {
        if (touchingPlatform) {
            audioManagerComp.JumpSound();
            rigidBody.AddForce(jumpImpulse, ForceMode.Impulse);
            touchingPlatform = false;
        }
    }

    void CheckCollision(Collision other)
    {
        Transform otherTransform = other.transform;
        float otherScaleZ;
        if (otherTransform.name == "DoubleBlock(Clone)")
        {
            otherScaleZ = 4;
        }
        else {
            otherScaleZ = 2;
        }
        if (System.Math.Abs(otherTransform.position.z - playerTransform.position.z) < playerTransform.localScale.z / 2 + otherScaleZ / 2 - 0.0211f)
        {
            rigidBody.AddForce(rebound, ForceMode.Impulse);
            CollisionWall();
        }
        else
        {
            fog.transform.parent = null;
            rigidBody.constraints = previousConstaints;
            startGame = false;
        }
    }

    void PlayerExplosion()
    {
        nGames++;
        PlayerPrefs.SetInt("NumberOfGames", PlayerPrefs.GetInt("NumberOfGames") + 1);
        deadPlayer.transform.position = playerTransform.position;
        gameObject.SetActive(false);
        audioManagerComp.DeadSound();
        GameObject temp = Instantiate(deadPlayer);
        foreach (Transform hit in temp.transform)
        {
            hit.gameObject.GetComponent<Rigidbody>().AddForce((hit.position - transform.position) * random.Next(900, 1200), ForceMode.Acceleration);
        }
    }

    void CollisionWall() {
        particlesTransform.position = new Vector3(playerTransform.position.x + 0.4f,
                                                   playerTransform.position.y,
                                                   playerTransform.position.z);
        GameObject temp = Instantiate(particles);
        Destroy(temp, 1f);
        foreach (Transform hit in temp.transform) {
            hit.gameObject.GetComponent<Rigidbody>().AddForce((hit.position - temp.transform.position) * random.Next(1000, 2500), ForceMode.Acceleration);
        }
    }

    public void StartGame()
    {
        startGame = true;
        gameScreen.SetActive(true);
    }

    public void StopGame()
    {
        startGame = false;
        fog.transform.parent = null;
        PlayerExplosion();
        ShowAds();
    }

    void ShowAds() {
        //nGame++;
        if (nGame >= 5 && PlayerPrefs.GetInt("BuyNoAds") != 1){
            AdsManager.showInterstitial = true;
            nGame = 1;
        }
        else {
            deadScreen.GetComponent<DeadScreenController>().View();
        }
    }

    void AddScore(int n)
    {
        score += n;
        scoreText.GetComponent<Text> ().text = score.ToString();
    }
}
