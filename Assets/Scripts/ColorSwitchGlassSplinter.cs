﻿using UnityEngine;

public class ColorSwitchGlassSplinter : MonoBehaviour {

    public Color colorB, colorT, colorEmB, colorEmT;
    public float timeSwitch;
    private float a = 0;
    Renderer splinterRenderer;

    void Start() {
        splinterRenderer = GetComponent<Renderer>();
    }

    void Update() {
        if (a <= timeSwitch) {
            splinterRenderer.material.color = Color.Lerp(colorB, colorT, a / timeSwitch);
            splinterRenderer.material.SetColor("_EmissionColor", Color.Lerp(colorEmB, colorEmT, a / timeSwitch));
            a += Time.deltaTime;
        }
    }
}
