﻿using UnityEngine;

public class GlassAudioManager : MonoBehaviour {

    public AudioClip[] glassSound = new AudioClip[6];
    private int nGlassSound = 0;
    private AudioSource audioSource;

    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    public void GlassSound()
    {
        if (AudioManager.onSound) {
            audioSource.clip = glassSound[nGlassSound];
            audioSource.Play();
            if (nGlassSound >= 5)
            {
                nGlassSound = 0;
            }
            else
            {
                nGlassSound++;
            }
        }
    }
}
