﻿using UnityEngine;

public class ColorSwitchWallController : MonoBehaviour {

    public GameObject wall, colorManager;
    public Color colorPlaneB, colorWallB, colorPlaneT, colorWallT;
    public float timeSwitch;
    float a = 10;
    Renderer wallRenderer, planeRenderer;

    void Start() {
        wallRenderer = wall.GetComponent<Renderer>();
        planeRenderer = GetComponent<Renderer>();
        colorManager = GameObject.Find("ColorManager");
    }

    void Update()
    {
        if (a <= timeSwitch)
        {
            a += Time.deltaTime;
            planeRenderer.material.color = Color.Lerp(colorPlaneT, colorPlaneB, a / timeSwitch);
            wallRenderer.material.color = Color.Lerp(colorWallT, colorWallB, a / timeSwitch);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.transform.name == "Player")
        {
            planeRenderer.material.color = colorPlaneT;
            wallRenderer.material.color = colorWallT;
            colorPlaneB = colorManager.GetComponent<ColorManager>().wallPlaneColors[ColorManager.nColor];
            colorWallB = colorManager.GetComponent<ColorManager>().wallColors[ColorManager.nColor];
            a = 0;
        }
    }
}
