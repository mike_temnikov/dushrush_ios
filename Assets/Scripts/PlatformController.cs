﻿using UnityEngine;

public class PlatformController : MonoBehaviour {

    public GameObject plane;
    bool touching = false;
    Transform platformTransform;
    Renderer platformRenderer;
    BoxCollider platformCollider;
    System.Random random = new System.Random();
    Vector3 platformScale = new Vector3(1, 1, 1), platformPosition = new Vector3(0, 0, 0);

    void Start() {
        platformTransform = transform;
        platformRenderer = GetComponent<Renderer>();
        platformCollider = GetComponent<BoxCollider>();
    }

    void Update() {
        if (touching) {
            platformScale.y = platformTransform.localScale.y;
            platformTransform.localScale = Vector3.Lerp(platformScale,
                                                        new Vector3(1, platformScale.y - 1f, 1),
                                                        Time.deltaTime);
        }

        if (platformTransform.localScale.y <= 0.1) {
            platformCollider.isTrigger = true;
            platformRenderer.enabled = false;
            plane.SetActive(false);
        }
    }

    void OnCollisionEnter(Collision other) {
        if (other.transform.name == "Player") {
            touching = true;
        }
    }

    void OnCollisionExit(Collision other) {
        if (other.transform.name == "Player") {
            touching = false;
        }
    }

    void OnTriggerEnter(Collider other) {
        if (other.name == "Invisible") {
            platformRenderer.enabled = true;
            platformCollider.isTrigger = false;
            plane.SetActive(true);
            platformScale.y = random.Next(1, 3);
            platformTransform.localScale = platformScale;
            platformPosition.x = platformTransform.position.x + 22.5f;
            platformTransform.position = platformPosition;
        }
    }
}
