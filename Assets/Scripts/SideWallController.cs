﻿using UnityEngine;

public class SideWallController : MonoBehaviour {

	public GameObject player;
	public float nextPosition; 
	void Start () {
	
	}

	void Update () {
		if (player.transform.position.x - transform.position.x > 3) {
			transform.position = new Vector3 (transform.position.x + nextPosition, transform.position.y, transform.position.z); 
		}
	}
}
