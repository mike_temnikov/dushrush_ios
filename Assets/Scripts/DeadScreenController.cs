﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class DeadScreenController : MonoBehaviour {

    public GameObject startScreen, scoreGui, topScoreGui, audioManager, pauseButton, particles1, player, firework, cameraObject, newBest;
    public float topScore;
    public GameObject gameSetting;
    //GameSettings gameSettings;
    Text topScoreGuiText, scoreGuiText;
    AudioManager audioManagerComp;
    System.Random random = new System.Random();

    void Initialization() {
        //gameSettings = gameSetting.GetComponent<GameSettings>();
        scoreGuiText = scoreGui.GetComponent<Text>();
        topScoreGuiText = topScoreGui.GetComponent<Text>();
        audioManagerComp = audioManager.GetComponent<AudioManager>();
    }

    public void View() {
        //PlayerPrefs.SetFloat("TopScore", 0f);
        gameObject.SetActive(true);
        pauseButton.SetActive(false);
        Initialization();
        topScoreGuiText.text = "TOP  " + PlayerPrefs.GetFloat("TopScore").ToString();
        //gameSettings.SetTop(Convert.ToInt32(scoreGui.GetComponent<Text>().text));
        //gameSettings.CheckAchivements(Convert.ToInt32(scoreGui.GetComponent<Text>().text));
        if (PlayerPrefs.GetFloat("TopScore") < float.Parse(scoreGuiText.text)) {
            PlayerPrefs.SetFloat("TopScore", float.Parse(scoreGuiText.text));
            topScoreGuiText.text = "BEST  " + PlayerPrefs.GetFloat("TopScore").ToString();
            newBest.SetActive(true);
            StartCoroutine(HighscoreNew());
        }
    }

	public void Skip () {
        gameObject.SetActive (false);
        pauseButton.SetActive(false);
    }

	public void Retry () {
        audioManagerComp.ButtonClickSound();
        SceneManager.LoadScene("Main");
        StartScreenController.retry = true;
	}

    IEnumerator HighscoreNew()
    {
        yield return new WaitForSeconds(UnityEngine.Random.Range(1, 1.5f));
        GameObject f = Instantiate(firework);
        f.GetComponent<DestroyController>().DestroyInit();
        f.transform.position = new Vector3(cameraObject.transform.position.x + 20f, 9 - UnityEngine.Random.Range(0, 3), UnityEngine.Random.Range(0, 10) - 5);
        f.SetActive(true);
        audioManagerComp.HighscoreSound();
        StartCoroutine(HighscoreNew());
    }
}
