﻿using UnityEngine;

public class BreakGlassController : MonoBehaviour 
{
	public float radius;
	public bool collision = false;
    public int power;
	public GameObject brokenObject, temp;
    private Vector3 vector;
    private System.Random random;

    void Start() {
        random = new System.Random();
    }

	void OnTriggerEnter(Collider collider)
	{	
		if (collider.gameObject.name == "Player" && !collision) {
			collision = true;
            GetComponent<MeshRenderer>().enabled = false;
			temp = Instantiate (brokenObject);
			temp.transform.position = new Vector3 (transform.position.x, 0, transform.position.z);
			Vector3 explosionPos = new Vector3(transform.position.x, 
                                               collider.gameObject.transform.position.y, 
                                               collider.gameObject.transform.position.z);
			Collider[] colliders = Physics.OverlapSphere (explosionPos, radius);
            vector = transform.position - collider.transform.position;
            foreach (Collider hit in colliders) {
				if (hit.GetComponent<Rigidbody> () && hit.gameObject.transform.name != "Player") {
                    Vector3 forceVector = new Vector3(System.Math.Abs(vector.x) + random.Next(5, 7),
                                                      System.Math.Abs(vector.z) + random.Next(1, 2),
                                                      System.Math.Abs(vector.y) + ((float)(random.NextDouble() * 2 - 1) * random.Next(0, 5)) * random.Next(1, power));
                    hit.GetComponent<Rigidbody>().AddForce(forceVector, ForceMode.Impulse);
					hit.GetComponent<Rigidbody> ().useGravity = true;
				}
			}
		}
	}
}
