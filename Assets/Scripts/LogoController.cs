﻿using UnityEngine;

public class LogoController : MonoBehaviour {

    public GameObject logo;
    static bool first = true;

    void Start() {
        if (first) {
            logo.SetActive(true);
            first = false;
        }
    }

    void Update() {
        if (Time.time > 5f) {
            logo.SetActive(false);
        }
    }
}
