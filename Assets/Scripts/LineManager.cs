﻿using UnityEngine;
using System.Collections.Generic;

public class LineManager : MonoBehaviour {

    public GameObject object0, object1, object2, object3, object4, player;
    public int amountLines;
    public Vector3 startPosition, nextPosition, distanceLines, speedWall;
    Vector3 scaleBlock = new Vector3(0.5f, 2.5f, 2f), scaleDoubleBlock = new Vector3(0.5f, 2.5f, 4f);
    private static string[] stringLines = {"0414433433441414",
                                           "33443343334434434334144",
                                           "134431433343344334",
                                           "30440304034443040344334",
                                           "3344334414143344334",
                                           "033441433443344304034",
                                           "03304344030440304334",
                                           "33443443340304403044334",
                                           "034430403334440330433443334",
                                           "33431344334403304434",
                                           "24144030433344333414144",
                                           "334414334141433440304",
                                           "141430344303414333444334",
                                           "3044033443304403343443134",
                                           "1414433344141433344",
                                           "3344303433433441431344314134",
                                           "0344304403304430344334304",
                                           "244244333443334403304",
                                           "0304030443134430340304",
                                           "34334144030443343344",
                                           "31441344334030433440304334"};
    private static GameObject[][] arrayObjects;
    private int coefficient = 1;
    private float[] lengthLines;
    private Queue<GameObject>[] wallQueue;

    void Start()
    {
        GenerateObjectArray(stringLines);
        wallQueue = new Queue<GameObject>[arrayObjects.Length];
        lengthLines = new float[arrayObjects.Length];
        GenerateLines();
    }

    void Update()
    {
        for (int i = 0; i < amountLines; i++)
        {
            if (wallQueue[i].Peek().transform.position.z < -lengthLines[i] / 2 && wallQueue[i].Peek().GetComponent<Rigidbody>().velocity.z < 0)
            {
                ReplaceWall(i, 1);
            }
            if (wallQueue[i].Peek().transform.position.z > lengthLines[i] / 2 && wallQueue[i].Peek().GetComponent<Rigidbody>().velocity.z > 0)
            {
                ReplaceWall(i, -1);
            }
            if (System.Math.Truncate(player.transform.position.x - wallQueue[i].Peek().transform.position.x) > 2)
            {
                if (wallQueue[i].Peek().GetComponent<Rigidbody>().velocity.z < 0)
                {
                    ReplaceLine(i, 1);
                }
                else
                {
                    ReplaceLine(i, -1);
                }

            }
        }
    }

    void GenerateObjectArray(string[] stringLines)
    {
        arrayObjects = new GameObject[stringLines.Length][];
        for (int i = 0; i < stringLines.Length; i++)
        {
            arrayObjects[i] = new GameObject[stringLines[i].Length];
            for (int j = 0; j < stringLines[i].Length; j++)
            {
                switch (stringLines[i][j])
                {
                    case '0':
                        arrayObjects[i][j] = object0;
                        break;
                    case '1':
                        arrayObjects[i][j] = object1;
                        break;
                    case '2':
                        arrayObjects[i][j] = object1;
                        break;
                    case '3':
                        arrayObjects[i][j] = object3;
                        break;
                    case '4':
                        arrayObjects[i][j] = object4;
                        break;
                }
            }
        }
    }

    void GenerateLines()
    {
        for (int i = 0; i < amountLines; i++)
        {
            coefficient = -coefficient;
            nextPosition = startPosition;
            nextPosition = new Vector3(nextPosition.x + distanceLines.x * i, nextPosition.y, nextPosition.z * coefficient);
            GenerateLine(nextPosition, i);
        }
    }

    void GenerateLine(Vector3 position, int nLine)
    {
        int randomLine = RandomNumber(arrayObjects.Length);
        wallQueue[nLine] = new Queue<GameObject>(arrayObjects[randomLine].Length);
        for (int i = 0; i < arrayObjects[randomLine].Length; i++)
        {
            GameObject temp = (GameObject)Instantiate(arrayObjects[randomLine][i]);
            wallQueue[nLine].Enqueue(temp);
            if (temp.name != "DoubleBlock(Clone)")
            {
                position = new Vector3(position.x, position.y, position.z + scaleBlock.z / 2 * coefficient);
                temp.transform.position = position;
                position = new Vector3(position.x, position.y, position.z + scaleBlock.z / 2 * coefficient);
                lengthLines[nLine] += scaleBlock.z;
            }
            else {
                position = new Vector3(position.x, position.y, position.z + scaleDoubleBlock.z / 2 * coefficient);
                temp.transform.position = position;
                position = new Vector3(position.x, position.y, position.z + scaleDoubleBlock.z / 2 * coefficient);
                lengthLines[nLine] += scaleDoubleBlock.z;
            } 
            temp.GetComponent<Rigidbody>().useGravity = false;
            temp.GetComponent<Rigidbody>().AddForce(speedWall * -coefficient, ForceMode.VelocityChange);
        }
    }

    int RandomNumber(int max)
    {
        System.Random random = new System.Random();
        return random.Next(max);
    }

    void ReplaceWall(int i, int coeff)
    {
        GameObject temp = wallQueue[i].Dequeue();
        temp.transform.position = new Vector3(temp.transform.position.x,
                                               temp.transform.position.y,
                                               temp.transform.position.z + (lengthLines[i]) * coeff);
        wallQueue[i].Enqueue(temp);
    }

    void ReplaceLine(int n, int coeff)
    {
        coefficient = coeff;
        Vector3 position = new Vector3(wallQueue[n].Peek().transform.position.x + amountLines * distanceLines.x,
                                        startPosition.y,
                                        startPosition.z * coeff);
        int t = wallQueue[n].Count;
        for (int i = 0; i < t; i++)
        {
            Destroy(wallQueue[n].Dequeue());
        }
        lengthLines[n] = 0;
        GenerateLine(position, n);
    }
}
