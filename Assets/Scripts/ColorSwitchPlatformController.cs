﻿using UnityEngine;

public class ColorSwitchPlatformController : MonoBehaviour {

    public GameObject platform, colorManager;
    public Color colorPlaneT, colorPlatformT, colorPlaneB, colorPlatformB;
    public float timeSwitch;
    public bool touching;
    private float a = 0;
    Renderer platformRenderer, planeRenderer;

    void Start() {
        platformRenderer = platform.GetComponent<Renderer>();
        planeRenderer = GetComponent<Renderer>();
        colorManager = GameObject.Find("ColorManager");
    }

    void Update()
    {
        if (touching && a <= timeSwitch)
        {
            a += Time.deltaTime;
            planeRenderer.material.color = Color.Lerp(colorPlaneT, colorPlaneB, a / timeSwitch);
            platformRenderer.material.color = Color.Lerp(colorPlatformT, colorPlatformB, a / timeSwitch);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.transform.name == "Player")
        {
            touching = true;
            planeRenderer.material.color = colorPlaneT;
            platformRenderer.material.color = colorPlatformT;
            colorPlaneB = colorManager.GetComponent<ColorManager>().platformPlaneColors[ColorManager.nColor];
            colorPlatformB = colorManager.GetComponent<ColorManager>().platformColors[ColorManager.nColor];
            a = 0;
        }
    }
}
