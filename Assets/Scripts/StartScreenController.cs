﻿using UnityEngine;

public class StartScreenController : MonoBehaviour {

    public GameObject player, audioManager;
    public static bool retry = false;

	public void View () {
        if (!retry) {
            gameObject.SetActive(true);
        }
        else {
            player.GetComponent<PlayerController>().StartGame();
        }
	}

	public void Hide () {
        gameObject.SetActive(false);
    }

	public void Click () {
        audioManager.GetComponent<AudioManager>().ButtonClickSound();
		Hide();
        player.GetComponent<PlayerController>().Jump();
        player.GetComponent<PlayerController>().StartGame ();
	}
}
